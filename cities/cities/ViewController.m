//
//  ViewController.m
//  cities
//
//  Created by Click Labs134 on 10/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *list;
NSDictionary *dict;
NSDictionary *dict1;
NSDictionary *dict2;
NSDictionary *dict3;
NSDictionary *dict4;
NSDictionary *dict5;
NSDictionary *dict6;
NSDictionary *dict7;
NSDictionary *dict8;
NSDictionary *dict9;
NSMutableArray *list2;
NSDictionary *dict10;
NSDictionary *dict11;
NSDictionary *dict12;
NSDictionary *dict13;
NSDictionary *dict14;
NSMutableArray *temp;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIButton *cities;
@property (strong, nonatomic) IBOutlet UIButton *nonvisited;
@property (strong, nonatomic) IBOutlet UIImageView *i1;

@property (strong, nonatomic) IBOutlet UIImageView *i2;
@property (strong, nonatomic) IBOutlet UITableView *table;
@end

@implementation ViewController
@synthesize cities;
@synthesize nonvisited;
@synthesize table;
@synthesize image;
int tag;
- (void)viewDidLoad {
    [super viewDidLoad];
    tag=0;
    
    list = [[NSMutableArray alloc ] init ];
     dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Aligarh",@"city",@"1",@"id", nil];
      [list addObject:dict];
    
    dict1=[NSDictionary dictionaryWithObjectsAndKeys:@"Bangalore",@"city",@"2",@"id", nil];
    [list addObject:dict1];
    
    dict2=[NSDictionary dictionaryWithObjectsAndKeys:@"Chandigarh",@"city",@"3",@"id", nil];
    [list addObject:dict2];
    
    dict3=[NSDictionary dictionaryWithObjectsAndKeys:@"Delhi",@"city",@"4",@"id", nil];
    [list addObject:dict3];

    dict4=[NSDictionary dictionaryWithObjectsAndKeys:@"Faridabad",@"city",@"5",@"id", nil];
        [list addObject:dict4];
    
    dict5=[NSDictionary dictionaryWithObjectsAndKeys:@"Mumbai",@"city",@"5",@"id", nil];
    [list addObject:dict5];

    dict6=[NSDictionary dictionaryWithObjectsAndKeys:@"Pune",@"city",@"5",@"id", nil];
    [list addObject:dict6];

    dict7=[NSDictionary dictionaryWithObjectsAndKeys:@"Punchkula",@"city",@"5",@"id", nil];
    [list addObject:dict7];

    dict8=[NSDictionary dictionaryWithObjectsAndKeys:@"Rohtak",@"city",@"5",@"id", nil];
    [list addObject:dict8];

    dict9=[NSDictionary dictionaryWithObjectsAndKeys:@"Surat",@"city",@"5",@"id", nil];
    [list addObject:dict9];

    
    list2 = [[NSMutableArray alloc ] init ];
    
    dict10=[NSDictionary dictionaryWithObjectsAndKeys:@"Aligarh",@"city",@"10",@"id", nil];
    [list2 addObject:dict10];
    
    dict11=[NSDictionary dictionaryWithObjectsAndKeys:@"Chandigarh",@"city",@"5",@"id", nil];
    [list2 addObject:dict11];

    dict12=[NSDictionary dictionaryWithObjectsAndKeys:@"Delhi",@"city",@"5",@"id", nil];
    [list2 addObject:dict12];

    dict13=[NSDictionary dictionaryWithObjectsAndKeys:@"Pune",@"city",@"5",@"id", nil];
    [list2 addObject:dict13];

    dict14=[NSDictionary dictionaryWithObjectsAndKeys:@"Rohtak",@"city",@"5",@"id", nil];
    [list2 addObject:dict14];

    
    temp=[[NSMutableArray alloc] init];
    
    [table reloadData];
  table.hidden=YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)action:(id)sender {
    if (([sender tag ]==1))        {
        [temp removeAllObjects];
        [temp addObjectsFromArray:list];
    }
    else if(([sender tag ]==2))        {
        [temp removeAllObjects];
        [temp addObjectsFromArray:list2];
    }
    table.hidden=NO;
        [table reloadData];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [temp count];
}


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView
                              dequeueReusableCellWithIdentifier:@"cellReuse"] ;
   
    cell.textLabel.text =  [[temp objectAtIndex:indexPath.row] objectForKey:@"city"];
    cell.detailTextLabel.text = [[temp objectAtIndex:indexPath.row] objectForKey:@"id"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    for(int i = 0;i<[list count];i++)
//        
//    {
//        
//        for(int j= 0;j<[list2 count];j++)
//            
//        {
//            
//            if([[list objectAtIndex:i] isEqualToString:[list2 objectAtIndex:j]])
//                
//            {
//                
//                  cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                
//                
//                
//            }
//            else{
//             cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//
//        }
//    
    
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
  
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
